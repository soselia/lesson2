import kotlin.math.sqrt
//შექმენით კლასი Point რომელსაც ექნება 2 ველი, შესაბამისად x და y კოორდინატის შესანახად 💾  (საწყისი მნიშვნელობები გადმოაწოდეთ
// კონსტრუქტორში). კლას უნდა ჰქონდეს შემდეგი ფუნქციონალი: 🧑🏼‍💻
//toString მეთოდი - რომელიც უნდა აბრუნებდეს კლასის ველებს String ფორმატში 👈
//equals მეთოდი - უნდა იძლეოდეს საშუალებას შევადაროთ 2 point კლასის ობიექტი 🤗
//მეთოდი, რომელებიც სათავის მიმართ სიმეტრიულად გადაიტანს წერტილს  👌🏻
//მეთოდი, რომელსაც ჩაეწოდება სხვა Point კლასის ობიექტი და დააბრუნებს მანძილს ამ ორ წერტილს შორის 🗺️ (optional)
fun main():Unit{
    val point = Point(5,7)
    println(point.toString())
    println(point.equals(point = Point(5,7)))
    println(point.length(point = Point(2,4)))
}

class Point(var x: Int = 0, var y: Int = 0){

    override fun toString():String{
        return "X- " + this.x + "; Y- " + this.y
    }

    fun equals(point: Point):Boolean{
        return point.x == this.x && point.y == this.y
    }
    fun symmetrical(x:Int = this.x, y: Int = this.y){
        this.x*=-1
        this.y*=-1
    }
    fun length(point: Point): Double {
        return sqrt(((point.x - this.x)*(point.x - this.x)+(point.y - this.y)*(point.y - this.y)).toDouble())
    }

}